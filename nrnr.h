#ifndef __NRNR_H__
#define __NRNR_H__


#include <stdlib.h>



struct NrNrHood {
    size_t numPoints;
    size_t dimensions;
    size_t chunks;
    const float* points;
    unsigned short* indices;
    unsigned short* nextIndices;
};


struct NrNrHood* NrNrCreateNeighborhood(size_t numPoints, size_t dimensions, const float* points);
struct NrNrHood* NrNrFreeNeighborhood(struct NrNrHood* hood);

size_t NrNrSearchBlock(const struct NrNrHood* hood, size_t** results, const float* minimum, const float* maximum);
size_t* NrNrFreeResults(size_t* results);


#endif // __NRNR_H__
